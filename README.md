# CRI Bioinformatics Training #

This repository contains example scripts from our various training sessions. To explore the available materials, please click the "source" link from the navigation bar on the left side of the page. For more info please see [our training homepage](http://cri.uchicago.edu/?page_id=1207)

# Available Training Files #

* Introduction to Python Programming, Pt. 1 ([wiki](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+I))

* Introduction to Python Programming, Pt. 2 ([wiki](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+II))