# Introduction to Python Part I Examples #

This directory contains example scripts we discussed in the training session.
Please see the [Introduction to Python Part I wiki page](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+I) for more information.

## first.py

You will likely not understand everything going on here, but the idea is to simply showcase what a basic python script looks like. Notice the comments, assignment of values to variables,
the use of white-space, etc.

## genetic_code.py

This script showcases Python [dictionaries](https://docs.python.org/2/library/stdtypes.html#typesmapping). We build a dictionary
containing the genetic code that can be used to convert codons to amino acids.

