'''
String substitution for introducing yourself
using python
'''

introFormat = '''
Hello! My name is {name} and I was born in the city
of {city}. I am really {level} with computers and want
to learn more!
'''

def tellIntro():
    userPicks = dict()
    addPick('your name', 'name', userPicks)
    addPick('your birth city', 'city', userPicks)
    addPick('Are you good or bad with computers?', 'level', userPicks)
    intro = introFormat.format(**userPicks) 
    print intro

def addPick(cue, key, dictionary):
    '''Prompt for a user response using the cue string,
    and place the key-response pair in the dictionary.
    '''
    prompt = 'Enter your response for ' + cue + ': '
    response = raw_input(prompt)
    dictionary[key] = response

tellIntro()
