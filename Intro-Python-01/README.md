# CRI Training: Introduction to Python Programming Part I #

This respository contains the examples scripts and challenge solutions presented in the training session.
Please see the [wiki page](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+I) for more information.

# Contents

The possible solutions to challenges are located in the `challenges` directory and the example scripts used in training are located in the `examples` directory.