'''Challenge 1'''

# Get the user's name
user_name = raw_input('Please enter your name: ')

# Get the number of students in the class
students = int(raw_input('How many students are in the class? '))

# Get the number of lecturers in the class
lecturers = raw_input('How many lecturers are in the class? ')

# calculate the student/teacher ratio
student_teacher_ratio = students / float(lecturers)

# Now, print to the screen
print "Hello, my name is ", user_name + "."
print "There are currently", students, "students in this class."
print "This class has", lecturers, "lecturers."
print "That means this class had a student-teacher ratio of:", student_teacher_ratio
