# Challenges for Introduction to Python Part I #

Here you will find possible solutions to the challenges we went over, as well as the take-home challenges. 
Please see the [Introduction to Python Part 1 wiki page](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+I) for more information.

## Challenge 1

_skills: standard input, standard output, basic math_

Create a python script that will ask the following questions:

* User's name
* Number of students in the class
* Number of lecturers in the class

Using this information calculate the student/teacher ratio and print all the information to the screen.

Your terminal should look similar to this:

```
Please enter your name: Kyle Hernandez
How many students are in the class? 20
How many lecturers are in the class? 3
Hello, my name is  Kyle Hernandez.
There are currently 20 students in this class.
This class has 3 lecturers.
That means this class had a student-teacher ratio of: 6.66666666667
```

## Challenge 2

_skills: functions, basic arithmetic, python data types_

Write a script that asks the user for three different numbers and returns the average of the numbers.

You **must create a function** to calculate the mean.

An example implementation would look like this:

```
Enter value 1: 13
Enter value 2: 9
Enter value 3: 4
The average of the three values is: 8.66666666667
```

## Challenge 3

_skills: dictionaries, lists, for loops, string concatenation_

* Write a script that converts these codons `['ACA', 'TCG', 'TCT', 'TGG', 'TAG']` into their amino acid sequence
* Create a single string with the amino-acid sequence
* Print the entire work flow
* You must use a dictionary, for-loop, and lists

Your output should look like this:

```
My codons:  ['ACA', 'TCG', 'TCT', 'TGG', 'TAG']
ACA -> T
TCG -> S
TCT -> S
TGG -> W
TAG -> _
Final amino acid sequence: TSSW_
```

*hint:* use the dictionary already created in the `genetic_code.py` example script