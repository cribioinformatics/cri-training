"""Challenge 2"""

def get_average( vals ):
   '''Calculates the average of three values'''
   ct = 0
   n  = 3
   for val in vals:
       ct = ct + val
   return ct / n

val_a = float(raw_input("Enter value 1: "))
val_b = float(raw_input("Enter value 2: "))
val_c = float(raw_input("Enter value 3: "))

val_list = [val_a, val_b, val_c]
print "The average of the three values is:", get_average(val_list)
