"""
Covert this list of codons into a single string of amino-acid sequences:

['ACA', 'TCG', 'TCT', 'TGG', 'TAG']

print out the work flow and then the amino-acid sequence
"""

# We can use this dictionary as provided in the examples
gencode = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W'}

# Create the list of codons
codons = ['ACA', 'TCG', 'TCT', 'TGG', 'TAG']
print "My codons: ", codons

# Create the variable for the final output as an empty string
amino_acid_sequence = ''

# Loop over the codons list, convert to amino acid, and concatenate the 
# amino_acid_sequence
for codon in codons:
    # Lookup the amino acid
    amino_acid = gencode[codon]
    print codon, "->", amino_acid
    # Concatenate with the amino_acid_sequence string
    amino_acid_sequence = amino_acid_sequence + amino_acid

# Print the amino-acid sequence
print "Final amino acid sequence:", amino_acid_sequence 
