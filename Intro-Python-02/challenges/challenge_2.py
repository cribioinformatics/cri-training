'''Challenge 2: Processing fasta file'''

fasta = "/group/cri-training/Python/Introduction/seq.fasta"
ofil  = "lower_seq.fasta"

with open(ofil, 'w') as o:
    with open(fasta, 'r') as f:
        for line in f:
            if line.startswith('>'): o.write(line)
            else:
                o.write(line.lower()) 
