"""Challenge 1: Sequence statistics"""

def estimate_base_frequency( seq ):
    '''Calculates the frequency of nucleotides within a sequence'''
    bases = ['A', 'C', 'T', 'G']
    uc_seq = seq.upper()
    print "The sequence " + seq + " has the following nucleotide frequencies:"
    for b in bases:
        print b + ':', uc_seq.count(b) / float(len(uc_seq))
    print "-"*30

dna_list = raw_input("Please enter a comma-separated list of DNA sequences: ").split(",")
for seq in dna_list:
    estimate_base_frequency(seq)
