'''Challenge 3: Reverse compliment of a fasta file'''

# Input fasta file path
fasta = "/group/cri-training/Python/Introduction/seq.fasta"

# Output fasta file path (in current working directory)
ofil  = "reverse_seq.fasta"

# Make a dictionary where the keys are the bases and the values are the 
# compliments
baseDict = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}

def revcomp(seq):
    '''Takes a DNA sequence and returns the reverse compliment.
       Like always, there are many ways to do this, and this is just
       one simple example using skills we have already learned.
    '''
    # We first need to reverse the string, but since reverse is a list
    # function we will need to convert the seq string back into a list
    # where each element is a base
    seqList = list(seq)
    # Lists are mutable, so we call the reverse() member function of lists
    # and it will actually change our seqList instance
    seqList.reverse()
    # Now we can loop over our seqList and base each base to the baseDict
    # to convert it to its compliment
    rev_comp_list = []
    for base in seqList:
        rev_comp_list.append(baseDict[base])
    # We now convert the rev_comp_list into a string and return it!
    return ''.join(rev_comp_list)
 
# Make the sequences list to hold each line of the fasta file since
# it is one long sequence split across multiple lines
sequences = []
fasta_row_length = 0

with open(ofil, 'w') as o:
    with open(fasta, 'r') as f:
        for line in f:
            # If its the header line, we want to simply add the string "| reverse compliment"
            if line.startswith('>'): 
                # We use the rstrip() string function to remove all whitespace
                # including the new line character and then concatenate the
                # text ' | reverse compliment\n' to the header. We need to add
                # the special character '\n' back to the end of the line, otherwise
                # the next o.write() statement will write to the same line!
                new_header = line.rstrip() + " | reverse compliment\n" 
                o.write(new_header)
            else:
                stripped_line = line.rstrip()
                # We need to get the length of the stripped_line and assign it to fasta_row_Length
                # only if it hasn't already been assigned!
                if not fasta_row_length: fasta_row_length = len(stripped_line)

                # Now we are on the sequence lines. We need to append the current line
                # with the new line character removed to the sequences list.
                sequences.append(stripped_line)

    # Now we have the sequences list with all of the lines of the fasta file,
    # We want to join the list into one long string, and then pass it to the
    # revcomp function
    reverseCompliment = revcomp(''.join(sequences))
    
    # Phew, ok, now we need to do one more challenging part. We want to format
    # the new fasta file the same way as the original one. Thus, we need to make sure
    # that the length of each sequence line is the same as the original. We have already
    # stored the length in the fasta_row_length. We can use the range function to provide
    # the appropriate indices for each start/stop position in the reverseCompliment string.
    # We will tell the range() function to give us the starting reverseCompliment index for 
    # each formatted fasta line. We tell the range() function to start at index 0 and to end
    # at the length of the reverseCompliment string, but offset by the fasta_row_length.
    # Here, the fasta_row_length is 70, and the length of reverseCompliment is 1159, so each 
    # iteration will assign the values:
    #    0, 70, 140, ..., 1120
    # These are our starting indices. To get the end index, we simply add the fast_row_length
    # to the current value of x. This works because Python is 0-indexed, so specifying the
    # coordinates: [0:70] would give you items from index 0 to 69, with a total length of 70. 
    for x in range(0, len(reverseCompliment), fasta_row_length):
        current_row = reverseCompliment[x:x+fasta_row_length]
        # Now we just write out the new line to the output file, but we need to
        # add the '\n' character
        o.write(current_row + '\n') 
