# Challenges for Introduction to Python Part II #

Here you will find possible solutions to the challenges we went over, as well as the take-home challenges.
Please see the [Introduction to Python Part 2 wiki page](https://wiki.uchicago.edu/display/CRIwksp/Introduction+to+Python+Programming%2C+Part+II) for more information.

## Challenge 1 ##

Write a script that asks the user to input a comma-separated list of DNA sequences and returns the following statistics:

* Length of each sequence
* Frequency of bases (e.g., A,C,T,G)

**Note: You must create a function to get the frequency of bases.**

An example implementation would look like this on the command line:

```
Please enter a comma-separated list of DNA sequences: ACGTTTT,AAAAAC,ATATATATATTTTTTTTTTTTTGGG
The sequence ACGTTTT has the following nucleotide frequencies:
A: 0.142857142857
C: 0.142857142857
T: 0.571428571429
G: 0.142857142857
------------------------------
The sequence AAAAAC has the following nucleotide frequencies:
A: 0.833333333333
C: 0.166666666667
T: 0.0
G: 0.0
------------------------------
The sequence ATATATATATTTTTTTTTTTTTGGG has the following nucleotide frequencies:
A: 0.2
C: 0.0
T: 0.68
G: 0.12
------------------------------
```

## Challenge 2 ##

Take the `seq.fasta` file and create a new fasta file (call it `lower_seq.fasta`) where the sequence has been changed to all lowercase characters. Do not change anything with the header line:

`>30179901_ref_NM_003107.2_Homo_sapiens`

The resulting file should look like:

```
>30179901_ref_NM_003107.2_Homo_sapiens
attggggtctgctctaagctgcagcaagagaaactgtgtgtgaggggaagaggcctgtttcgctgtcggg
tctctagttcttgcacgctctttaagagtctgcactggaggaactcctgccattaccagctcccttcttg
cagaagggagggggaaacatacatttattcatgccagtctgttgcatgcaggctttttggcttcctacct
tgcaacaaaataattgcaccaactccttagtgccgattccgcccacagagagtcctggagccacagtctt
ttttgctttgcattgtaggagagggactaagtgctagagactatgtcgctttcctgagctaccgagagcg
ctcgtgaactggaatcaactgcttcagggaaaaagaaaaaaaaaaaaaaaagacttgcctgggaggccgc
gagaaacttgcattggaagcttcagcaaccagcattcgagaaactcctctctactttagcacggtctcca
gactcagccgagagacagcaaactgcagcgcggtgagagagcgagagagagggagagagagactctccag
cctgggaactataactcctctgcgagaggcggagaactccttccccaaatcttttggggacttttctctc
tttacccacctccgcccctgcgaggagttgaggggccagttcggccgccgcgcgcgtcttcccgttcggc
gtgtgcttggcccggggaaccgggagggcccggcgatcgcgcggcggccgccgcgagggtgtgagcgcgc
gtgggcgcccgccgagccgaggccatggtgcagcaaaccaacaatgccgagaacacggaagcgctgctgg
ccggcgagagctcggactcgggcgccggcctcgagctgggaatcgcctcctcccccacgcccggctccac
cgcctccacgggcggcaaggccgacgacccgagctggtgcaagaccccgagtgggcacatcaagcgaccc
atgaacgccttcatggtgtggtcgcagatcgagcggcgcaagatcatggagcagtcgcccgacatgcaca
acgccgagatctccaagcggctgggcaaacgctggaagctgctcaaagacagcgacaagatccctttcat
tcgagaggcggagcggctgcgcctcaagcacatggctga
```

## Challenge 3 ##

Take the `seq.fasta` file and create a new fasta file (call it `reverse_seq.fasta`) containing the reverse compliment of the sequence. The output header line should be changed to state that it is the reverse compliment and
each sequence line should be the same length as the original file.

A resulting file should look like:

```
>30179901_ref_NM_003107.2_Homo_sapiens | reverse compliment
TCAGCCATGTGCTTGAGGCGCAGCCGCTCCGCCTCTCGAATGAAAGGGATCTTGTCGCTGTCTTTGAGCA
GCTTCCAGCGTTTGCCCAGCCGCTTGGAGATCTCGGCGTTGTGCATGTCGGGCGACTGCTCCATGATCTT
GCGCCGCTCGATCTGCGACCACACCATGAAGGCGTTCATGGGTCGCTTGATGTGCCCACTCGGGGTCTTG
CACCAGCTCGGGTCGTCGGCCTTGCCGCCCGTGGAGGCGGTGGAGCCGGGCGTGGGGGAGGAGGCGATTC
CCAGCTCGAGGCCGGCGCCCGAGTCCGAGCTCTCGCCGGCCAGCAGCGCTTCCGTGTTCTCGGCATTGTT
GGTTTGCTGCACCATGGCCTCGGCTCGGCGGGCGCCCACGCGCGCTCACACCCTCGCGGCGGCCGCCGCG
CGATCGCCGGGCCCTCCCGGTTCCCCGGGCCAAGCACACGCCGAACGGGAAGACGCGCGCGGCGGCCGAA
CTGGCCCCTCAACTCCTCGCAGGGGCGGAGGTGGGTAAAGAGAGAAAAGTCCCCAAAAGATTTGGGGAAG
GAGTTCTCCGCCTCTCGCAGAGGAGTTATAGTTCCCAGGCTGGAGAGTCTCTCTCTCCCTCTCTCTCGCT
CTCTCACCGCGCTGCAGTTTGCTGTCTCTCGGCTGAGTCTGGAGACCGTGCTAAAGTAGAGAGGAGTTTC
TCGAATGCTGGTTGCTGAAGCTTCCAATGCAAGTTTCTCGCGGCCTCCCAGGCAAGTCTTTTTTTTTTTT
TTTTCTTTTTCCCTGAAGCAGTTGATTCCAGTTCACGAGCGCTCTCGGTAGCTCAGGAAAGCGACATAGT
CTCTAGCACTTAGTCCCTCTCCTACAATGCAAAGCAAAAAAGACTGTGGCTCCAGGACTCTCTGTGGGCG
GAATCGGCACTAAGGAGTTGGTGCAATTATTTTGTTGCAAGGTAGGAAGCCAAAAAGCCTGCATGCAACA
GACTGGCATGAATAAATGTATGTTTCCCCCTCCCTTCTGCAAGAAGGGAGCTGGTAATGGCAGGAGTTCC
TCCAGTGCAGACTCTTAAAGAGCGTGCAAGAACTAGAGACCCGACAGCGAAACAGGCCTCTTCCCCTCAC
ACACAGTTTCTCTTGCTGCAGCTTAGAGCAGACCCCAAT
```

Some hints:

* You could make a dictionary to look up the compliment base (e.g., `baseDict = {'A': 'T', 'T':'A', 'C':'G', 'G': 'C'}`)
* Use the `reverse()` member function of Python lists
* Use the `range(start, stop, offset)` function for generating the appropriate start indices of each formatted fasta line
* You will need to remove the new line character `\n` from each line before processing. Fortunately, python has a built-in member-function for strings called `rstrip()`:
```
with open("myfile.txt", "r") as f:
    for line in f:
	    my_stripped_line = line.rstrip()
```
* Remember that the `write()` function doesn't automatically add the new line character (like the `print` function does), so you will need to add a `\n` to the end of each line you write to the new file!