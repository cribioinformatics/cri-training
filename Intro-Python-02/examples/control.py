'''Learning about if-else loops'''

lst = range(10)

for x in lst:
    if x < 2:
        print x, "is less than", 2
    elif x == 3:
        print x, "equals", 3
    elif x >= 5 and x != 8:
        print x, "is >= 5 and not 8"
    else:
        print x, "doesn't pass any other tests"
