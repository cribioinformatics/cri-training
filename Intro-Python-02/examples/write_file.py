"""Writing to a file"""

# Make a string pointing to the file we want to WRITE
ofil = "test_write.txt"

# Create a file object with mode 'w'. This will overwrite existing file
o = open(ofil, 'w')

# Write to the file
o.write("This is test line number 1\n")  # We need to add the new line character!!
o.write("This\tis\ttest\t2\n")           # We add in the tab character

# Close
o.close()
